<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
Created by IntelliJ IDEA.
User: siroj
Date: 8/19/2023
Time: 9:33 PM
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Download</title>
</head>
<body>
<c:forEach var="item" items="${requestScope.get('files')}">

    <c:out value="${item.name}"/>
    <a href="${pageContext.request.contextPath}/download?id=${item.id}">Download</a>
    <br> <br>
</c:forEach>

</body>
</html>
