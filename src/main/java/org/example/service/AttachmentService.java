package org.example.service;

import org.example.model.Attachment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface AttachmentService {

    void saveFile(HttpServletRequest request);

    void getFileById(Integer id, HttpServletResponse response);

    List<Attachment> getFiles();
}
