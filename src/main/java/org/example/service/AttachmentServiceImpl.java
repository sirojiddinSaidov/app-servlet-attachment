package org.example.service;

import org.example.model.Attachment;
import org.example.model.AttachmentContent;
import org.postgresql.core.SqlCommand;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AttachmentServiceImpl implements AttachmentService {

    @Override
    public void saveFile(HttpServletRequest request) {
        try {
            Part filePart = request.getPart("ketmon");
            Attachment attachment = Attachment.builder()
                    .name(filePart.getSubmittedFileName())
                    .size(filePart.getSize())
                    .contentType(filePart.getContentType())
                    .build();
            Integer attachmentId = saveAttachmentDB(attachment);
            AttachmentContent attachmentContent = AttachmentContent
                    .builder()
                    .content(filePart.getInputStream().readAllBytes())
                    .attachmentId(attachmentId)
                    .build();

            saveAttachmentContentDB(attachmentContent);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void getFileById(Integer id, HttpServletResponse response) {
        try {
            Connection connection = DBService.connection();
            PreparedStatement ps = connection.prepareStatement("SELECT ac.content, a.name, a.content_type, a.size FROM attachment_content ac JOIN attachment a ON a.id = ac.attachment_id WHERE a.id = ?");
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                byte[] bytes = resultSet.getBytes(1);
                String name = resultSet.getString(2);
                String contentType = resultSet.getString(3);
                long size = resultSet.getLong(4);
                response.setContentLength((int) size);
                response.setContentType(contentType);
                response.setHeader("Content-disposition", "inline; filename=\"" + name + "\"");
                ServletOutputStream outputStream = response.getOutputStream();
                outputStream.write(bytes);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public List<Attachment> getFiles() {
        try {
            Connection connection = DBService.connection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM attachment");
            List<Attachment> attachments = new ArrayList<>();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                long size = resultSet.getLong("size");
                String contentType = resultSet.getString("content_type");
                attachments.add(Attachment.builder()
                        .size(size)
                        .name(name)
                        .id(id)
                        .contentType(contentType)
                        .build());
            }
            System.out.println(attachments);
            return attachments;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    private Integer saveAttachmentDB(Attachment attachment) {
        Connection connection = DBService.connection();
        try {
            CallableStatement callableStatement = connection.prepareCall("{CALL saveAttachment(?,?,?)}");
            callableStatement.setString(1, attachment.getName());
            callableStatement.setLong(2, attachment.getSize());
            callableStatement.setString(3, attachment.getContentType());
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.execute();
            return callableStatement.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void saveAttachmentContentDB(AttachmentContent attachmentContent) {
        Connection connection = DBService.connection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO attachment_content(attachment_id, content) VALUES (?,?)");
            preparedStatement.setInt(1, attachmentContent.getAttachmentId());
            preparedStatement.setBytes(2, attachmentContent.getContent());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
