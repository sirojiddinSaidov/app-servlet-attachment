package org.example.service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.servlet.ServletContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBService {
    public static ServletContext servletContext;


    public static Connection connection() {

        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(
                    servletContext.getInitParameter("dbUrl"),
                    servletContext.getInitParameter("dbUser"),
                    servletContext.getInitParameter("dbPassword")
            );
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
