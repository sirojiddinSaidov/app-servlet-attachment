package org.example.controller;

import org.example.service.AttachmentService;
import org.example.service.AttachmentServiceImpl;
import org.example.service.DBService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig
@WebServlet("/upload")
public class FileUploadServlet extends HttpServlet {

    private AttachmentService attachmentService = new AttachmentServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DBService.servletContext = getServletContext();

        resp.sendRedirect("upload.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        attachmentService.saveFile(req);
//        req.setAttribute("files", attachmentService.getFiles());
        resp.sendRedirect("/download");

    }
}