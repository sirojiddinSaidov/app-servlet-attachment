package org.example.controller;

import org.example.service.AttachmentService;
import org.example.service.AttachmentServiceImpl;
import org.example.service.DBService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet("/download")
public class DownloadServlet extends HttpServlet {

    AttachmentService attachmentService = new AttachmentServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (DBService.servletContext == null)
            DBService.servletContext = getServletContext();

        String id = req.getParameter("id");
        if (id != null) {
            attachmentService.getFileById(Integer.valueOf(id), resp);
        } else {
            req.setAttribute("files", attachmentService.getFiles());
            req.getRequestDispatcher("download.jsp").include(req, resp);
        }
    }

//
//    private List<String> getFileNames() {
//        File file = new File("upload");
//        List<String> names = new ArrayList<>();
//        getFileNames(file, names, file.getName());
//        return names;
//    }
//
//    private void getFileNames(File file, List<String> names, String prefix) {
//        File[] files = file.listFiles();
//        for (File item : files) {
//            String temp = prefix + "/" + item.getName();
//            if (item.isFile())
//                names.add(temp);
//            else
//                getFileNames(item, names, temp);
//        }
//    }
}
