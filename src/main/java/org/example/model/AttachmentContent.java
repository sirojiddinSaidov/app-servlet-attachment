package org.example.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AttachmentContent {
    private byte[] content;

    private Integer attachmentId;
}
