package org.example.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class Attachment {

    private Integer id;

    private String name;

    private long size;

    private String contentType;
}
